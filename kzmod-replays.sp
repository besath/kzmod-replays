
#define DEBUG

#define PLUGIN_NAME				"KZMOD Replays"
#define PLUGIN_AUTHOR			"GameChaos"
#define PLUGIN_DESCRIPTION		""
#define PLUGIN_VERSION			"1.01"
#define PLUGIN_URL				""

#define REPLAY_BLOCK_SIZE		8
#define REPLAY_FMT_VERSION		100
#define REPLAY_MAGIC_NUMBER		0x420
#define REPLAY_PATH				"addons/sourcemod/data/kzmod-replays"
#define REPLAY_PATH_SM			"data/kzmod-replays"

#define BREATHER_TIME			1.0

#define DEFAULT_WEAPON			"weapon_usp"

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <regex>
#include <kreedzclimbing>
#include <kzmod-replays/stocks>

#pragma semicolon 1
#pragma dynamic 131072
#pragma newdecls required

enum 
{
	ReplayInfo_szCourse,
	ReplayInfo_iTeleports,
	ReplayInfo_szName,
	ReplayInfo_iTime
}

ArrayList g_alRecording[MAXPLAYERS + 1];
ArrayList g_alReplay;
ArrayList g_alCourses;
StringMap g_smCourseTimes;

char g_szMap[128];

float g_fBreatherStartTime;

int g_iTimerRunning[MAXPLAYERS + 1];

int g_iPlaybackTick;
int g_iReplayBot = -1;

bool g_bInvalidTimer[MAXPLAYERS + 1];
bool g_bRecordRun[MAXPLAYERS + 1];
bool g_bTPRun[MAXPLAYERS + 1];
bool g_bShowTP[MAXPLAYERS + 1];

bool g_bBotDataLoaded;
bool g_bInBreather;
bool g_bPlayReplay;

// replay info
char g_szReplayCourse[MAX_COURSE_SIZE];
char g_szReplayPlayerName[MAX_NAME_LENGTH];
int g_iReplayTeleports;
int g_iReplayTime;

#include "kzmod-replays/events.sp"
#include "kzmod-replays/playback.sp"
#include "kzmod-replays/recording.sp"

public Plugin myinfo =
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
};

public void OnPluginStart()
{
	RegConsoleCmd("sm_replay", Command_Replay);
	
	OnPluginStart_Events();
}

public void OnMapStart()
{
	GetCurrentMap(g_szMap, sizeof(g_szMap));
	CurrentMapCourses(g_alCourses);
	CourseTimes(g_smCourseTimes);
	
	int flags = GetCommandFlags("bot_add") & ~(FCVAR_CHEAT | FCVAR_SPONLY);
	SetCommandFlags("bot_add", flags);
	
	SpawnBot();
}

public void OnClientDisconnect(int client)
{
	if (g_alRecording[client] != null)
	{
		g_alRecording[client].Clear();
	}
	
	if (client == g_iReplayBot)
	{
		g_iReplayBot = -1;
	}
}

// ========
// Commands
// ========

public Action Command_Replay(int client, int args)
{
	// make sure the bot exists
	if (g_iReplayBot == -1)
	{
		SpawnBot();
	}
	
	ShowMenuTimetype(client);
	
	return Plugin_Handled;
}

// =====
// Menus
// =====

public void ShowMenuTimetype(int client)
{
	Menu menu = new Menu(MenuTimetype, MENU_ACTIONS_ALL);
	menu.SetTitle("%s", "TP or PRO?");
	
	menu.AddItem("0", "TP");
	menu.AddItem("1", "PRO");
	
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

public int MenuTimetype(Menu menu, MenuAction action, int param1, int param2)
{
	switch (action)
	{
		case MenuAction_Select:
		{
			char szInfo[32];
			menu.GetItem(param2, szInfo, sizeof(szInfo));
			int choice = StringToInt(szInfo);
			
			if (choice == 0)
			{
				g_bShowTP[param1] = true;
				ShowMenuReplay(param1);
			}
			if (choice == 1)
			{
				g_bShowTP[param1] = false;
				ShowMenuReplay(param1);
			}
		}
		case MenuAction_End:
		{
			delete menu;
		}
	}
	
	return 0;
}

public void ShowMenuReplay(int client)
{
	Menu menu = new Menu(MenuReplay, MENU_ACTIONS_ALL);
	char szMenuTitle[128];
	FormatEx(szMenuTitle, sizeof(szMenuTitle), "Replays for %s\nCourses:", g_szMap);
	menu.SetTitle("%s", szMenuTitle);
	
	char szInfo[4];
	char szCourse[MAX_COURSE_SIZE];
	
	for (int i; i < g_alCourses.Length; i++)
	{
		IntToString(i, szInfo, sizeof(szInfo));
		g_alCourses.GetString(i, szCourse, sizeof(szCourse));
		
		char szKeyName[MAX_COURSE_SIZE];
		FormatEx(szKeyName, sizeof(szKeyName), "%s%s", szCourse, g_bShowTP[client] ? "TP" : "PRO");
		
		int time;
		g_smCourseTimes.GetValue(szKeyName, time);
		
		char szTime[16];
		FormatRuntime(time, szTime, sizeof(szTime));
		Format(szCourse, sizeof(szCourse), "%s %s", szCourse, szTime);
		
		menu.AddItem(szInfo, szCourse);
	}
	
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

public int MenuReplay(Menu menu, MenuAction action, int param1, int param2)
{
	switch (action)
	{
		case MenuAction_Select:
		{
			char szInfo[32];
			menu.GetItem(param2, szInfo, sizeof(szInfo));
			int courseCount = g_alCourses.Length;
			int choice = StringToInt(szInfo);
			
			if (choice >= 0 && choice < courseCount)
			{
				char szCourse[MAX_COURSE_SIZE];
				g_alCourses.GetString(choice, szCourse, sizeof(szCourse));
				
				if (LoadReplay(szCourse, g_bShowTP[param1]))
				{
					// some code by danzay
					// Join spectators and spec the bot
					MoveToSpec(param1);
					
					CreateTimer(0.25, Timer_SpectateBot, GetClientUserId(param1));
					SetBotName(g_iReplayBot);
					g_bPlayReplay = true;
					
					char szTime[32];
					FormatRuntime(g_iReplayTime, szTime, sizeof(szTime));
					
					PrintToChat(param1, "[KZ] Playing %s[%s] by %s in (%s)", g_szMap, szCourse, g_szReplayPlayerName, szTime);
				}
				else
				{
					PrintToChat(param1, "[KZ] %s Replay for %s[%s] not found", g_bShowTP[param1] ? "TP" : "PRO", g_szMap, szCourse);
					ShowMenuReplay(param1);
				}
			}
			else
			{
				PrintToServer("[kzmod-replays] menu MenuReplay Invalid menu choice %i", choice);
			}
		}
		case MenuAction_End:
		{
			delete menu;
		}
	}
	
	return 0;
}

// ======
// Timers
// ======

// stolen/borrowed from danzay http://bitbucket.org/kztimerglobalteam/gokz/
public Action Timer_SpectateBot(Handle timer, int data)
{
	int client = GetClientOfUserId(data);
	
	if (IsValidClient(client) && IsValidClient(g_iReplayBot))
	{
		SetEntProp(client, Prop_Send, "m_iObserverMode", 4);
		SetEntPropEnt(client, Prop_Send, "m_hObserverTarget", g_iReplayBot);
	}
	return Plugin_Continue;
}

// =======
// Private
// =======

void SpawnBot()
{
	for (int i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i) && IsFakeClient(i))
		{
			KickClient(i);
		}
	}
	
	ServerCommand("bot_add;bot_zombie 1;bot_attack 0");
}

// TODO fix the shitty temporary name maybe
bool IsPlayerInCrouchTunnel(int client, const float vecPos[3])
{
    static const float vecMins[] = { -16.0, -16.0, 0.0 };
    static const float vecMaxs[] = { 16.0, 16.0, 54.0 }; // 54.0 is the normal ducked height in csgo

    TR_TraceHullFilter(vecPos, vecPos, vecMins, vecMaxs, MASK_SOLID, TraceFilter_IgnorePlayer, client);

    return (TR_DidHit());
}

	/**
	* Finds the lowest time in a folder of replays
	*
	* @param szReplayName	example: "CourseName_PRO_"
	* @return				Time in centiseconds
	*/
int FindLowestTime(char[] path, char[] szReplayName, bool highest = false)
{
	bool found;
	char szFileBuffer[MAX_COURSE_SIZE + 64]; // 64 for map name, "PRO"/"TP" and file extension
	DirectoryListing dL = OpenDirectory(path);
	if (dL == null)
	{
		return -1;
	}
	int maxFiles = 64;
	int fileCounter;
	int lowestTime = ~(1 << 31);
	int highestTime = 0;
	
	Regex regexReplayTime = CompileRegex(REGEX_REPLAY_TIME, PCRE_UCP);
	
	while (dL.GetNext(szFileBuffer, sizeof(szFileBuffer)) && fileCounter < maxFiles)
	{
		// 2 first items are "." and ".."
		if (fileCounter > 1)
		{
			if (strncmp(szFileBuffer, szReplayName, strlen(szReplayName)) == 0)
			{
				int captures = regexReplayTime.Match(szFileBuffer);
				if (captures > 0)
				{
					char szBuffer[32];
					regexReplayTime.GetSubString(1, szBuffer, sizeof(szBuffer));
					
					int time = StringToInt(szBuffer);
					
					if (!highest)
					{
						if (time < lowestTime)
						{
							lowestTime = time;
							found = true;
						}
					}
					else
					{
						if (time > highestTime)
						{
							highestTime = time;
							found = true;
						}
					}
				}
			}
		}
		fileCounter++;
	}
	
	if (!found)
	{
		return -1;
	}
	
	if (!highest)
	{
		return lowestTime;
	}
	else
	{
		return highestTime;
	}
}

int FindHighestTime(char[] path, char[] szReplayName)
{
	return FindLowestTime(path, szReplayName, true);
}

void SetBotName(int client)
{
	if (client == -1)
	{
		LogError("[kzmod-replays] Can't set bot name because it hasn't been spawned.");
		return;
	}
	// name
	char szTime[32];
	FormatRuntime(g_iReplayTime, szTime, sizeof(szTime));
	
	char szName[MAX_NAME_LENGTH];
	FormatEx(szName, sizeof(szName), "[%s] %s by %s", g_iReplayTeleports ? "TP" : "PRO", szTime, g_szReplayPlayerName);
	SetClientInfo(client, "name", szName);
}

void MoveToSpec(int client)
{
	if (!IsClientObserver(client))
	{
		FakeClientCommand(client, "forcespectate");
	}
}

void MoveToClimber(int client)
{
	if (IsClientObserver(client))
	{
		FakeClientCommand(client, "ct");
	}
}

void SetDefaultBotName(int client)
{
	SetClientInfo(client, "name", "Type !replay to watch replays");
}

// copies the times of replay bots into a hash map
// syntax for keys is [course name][tp ? "TP" : "PRO"], e.g. "xmas2008PRO"
void CourseTimes(StringMap &courseTimes)
{
	if (courseTimes == null)
	{
		courseTimes = new StringMap();
	}
	else
	{
		courseTimes.Clear();
	}
	
	char szReplayName[MAX_COURSE_SIZE + 64];
	char szKeyName[MAX_COURSE_SIZE];
	char path[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path, sizeof(path), "%s/%s", REPLAY_PATH_SM, g_szMap);
	
	int replayTime;
	for (int i; i < g_alCourses.Length; i++)
	{
		for (int tp; tp < 2; tp++)
		{
			g_alCourses.GetString(i, szKeyName, sizeof(szKeyName));
			Format(szReplayName, sizeof(szReplayName), "%s_%s_", szKeyName, tp ? "TP" : "PRO");
			
			// if endurance
			if (view_as<bool>(g_alCourses.Get(i, ByteCountToCells(MAX_COURSE_SIZE))))
			{
				replayTime = FindHighestTime(path, szReplayName);
			}
			else
			{
				replayTime = FindLowestTime(path, szReplayName);
			}
			
			Format(szKeyName, sizeof(szKeyName), "%s%s", szKeyName, tp ? "TP" : "PRO");
			courseTimes.SetValue(szKeyName, replayTime);
		}
	}
}

// ==============
// uh other stuff
// ==============

public void OnPlayerRunCmdPost(int client, int buttons, int impulse, const float vel[3], const float angles[3], int weapon, int subtype, int cmdnum, int tickcount, int seed, const int mouse[2])
{
	if (!IsValidClient(client) || !IsPlayerAlive(client) || IsFakeClient(client))
	{
		return;
	}
	
	OnPlayerRunCmdPost_Recording(client, buttons, angles);
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if (IsFakeClient(client) && IsPlayerAlive(client))
	{
		if (g_iReplayBot != -1)
		{
			OnPlayerRunCmd_Playback(client, buttons);
		}
	}
	
	return Plugin_Changed;
}