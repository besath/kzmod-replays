// "blocks2006_PRO_2725.kzreplay" gets the millisecond time from replay names
#define REGEX_REPLAY_TIME		"_(\\d+)\\.kzreplay"
#define RPBUTTONS_BITFILTER		(IN_DUCK | IN_FORWARD | IN_BACK | IN_MOVELEFT | IN_MOVERIGHT | IN_JUMP | IN_USE)

bool LoadReplay(char[] szCourse, bool tpRun)
{
	// thanks danzay for the code
	char path[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path, sizeof(path), "%s/%s", REPLAY_PATH_SM, g_szMap);
	
	// without .kzreplay file extension
	char szReplayName[MAX_COURSE_SIZE + 64];
	Format(szReplayName, sizeof(szReplayName), "%s_%s_", szCourse, tpRun ? "TP" : "PRO");
	
	if (!DirExists(path))
	{
		LogError("Path doesn't exist: \"%s\".", path);
		return false;
	}
	else
	{
		bool endurance = IsCourseEndurance(szCourse);
		int replayTime;
		
		if (!endurance)
		{
			replayTime = FindLowestTime(path, szReplayName);
			
			if (replayTime == -1)
			{
				return false;
			}
		}
		else
		{
			replayTime = FindHighestTime(path, szReplayName);
			
			if (replayTime == -1)
			{
				return false;
			}
		}
		
		Format(path, sizeof(path), "%s/%s%i.kzreplay", path, szReplayName, replayTime);
	}
	
	if (!FileExists(path))
	{
		LogError("Failed to load file: \"%s\".", path);
		return false;
	}
	
	File file = OpenFile(path, "rb");
	
	// Check magic number in header
	// file.WriteInt32(REPLAY_MAGIC_NUMBER);
	int magicNumber;
	file.ReadInt32(magicNumber);
	if (magicNumber != REPLAY_MAGIC_NUMBER)
	{
		LogError("Failed to load invalid replay file: \"%s\".", path);
		delete file;
		return false;
	}
	
	int length;
	
	// Check replay format version
	// file.WriteInt8(REPLAY_FMT_VERSION);
	int formatVersion;
	file.ReadInt8(formatVersion);
	if (formatVersion != REPLAY_FMT_VERSION)
	{
		LogError("Failed to load replay file with unsupported format version: \"%s\".", path);
		delete file;
		return false;
	}
	
	file.ReadInt8(length);
	char[] mapName = new char[length + 1];
	file.ReadString(mapName, length, length);
	mapName[length] = '\0';
	
	file.ReadInt8(length);
	file.ReadString(g_szReplayCourse, sizeof(g_szReplayCourse), length);
	g_szReplayCourse[length] = '\0';
	
	file.ReadInt32(g_iReplayTeleports);
	
	int steamAccountID;
	file.ReadInt32(steamAccountID);
	
	file.ReadInt8(length);
	char[] szSteamID2 = new char[length + 1];
	file.ReadString(szSteamID2, length, length);
	szSteamID2[length] = '\0';
	
	file.ReadInt8(length);
	file.ReadString(g_szReplayPlayerName, length, length);
	g_szReplayPlayerName[length] = '\0';
	
	int arrayLength;
	file.ReadInt32(arrayLength);
	
	int unixTime;
	file.ReadInt32(unixTime);
	
	file.ReadInt32(g_iReplayTime);
	
	// Setup playback tick data array list
	if (g_alReplay == null)
	{
		g_alReplay = new ArrayList(REPLAY_BLOCK_SIZE, arrayLength);
	}
	else
	{  // Make sure it's all clear and the correct size
		g_alReplay.Clear();
		g_alReplay.Resize(arrayLength);
	}
	
	any tickData[REPLAY_BLOCK_SIZE];
	for (int i = 0; i < arrayLength; i++)
	{
		file.Read(tickData, REPLAY_BLOCK_SIZE, 4);
		g_alReplay.Set(i, view_as<float>(tickData[0]), 0); // origin[0]
		g_alReplay.Set(i, view_as<float>(tickData[1]), 1); // origin[1]
		g_alReplay.Set(i, view_as<float>(tickData[2]), 2); // origin[2]
		g_alReplay.Set(i, view_as<float>(tickData[3]), 3); // angles[0]
		g_alReplay.Set(i, view_as<float>(tickData[4]), 4); // angles[1]
		g_alReplay.Set(i, view_as<float>(tickData[5]), 5); // angles[2]
		g_alReplay.Set(i, view_as<int>(tickData[6]), 6); // buttons
		g_alReplay.Set(i, view_as<int>(tickData[7]), 7); // flags
	}
	
	g_iPlaybackTick = 0;
	g_bBotDataLoaded = true;
	
	//int temptick = 10;
	//PrintToServer("magic number %i|fmt version %i|map %s|course %s|tp run %i|account id %i|steam id %s|name %s|tickcount %i|unix time %i| time %i", magicNumber, formatVersion, mapName, szReplayCourse, teleportsUsed, steamAccountID, szSteamID2, szAlias, arrayLength, unixTime, timeInCentiSeconds);
	
	delete file;
	return true;
}

public void OnPlayerRunCmd_Playback(int client, int &buttons)
{
	if (!g_bPlayReplay)
	{
		return;
	}
	
	if (g_alReplay.Length == 0)
	{
		return;
	}
	
	if (g_iReplayBot != client)
	{
		return;
	}
	
	if (!g_bBotDataLoaded)
	{
		return;
	}
	
	if (g_iPlaybackTick != 0 && g_iPlaybackTick >= g_alReplay.Length)
	{
		g_iPlaybackTick = 0;
		g_bBotDataLoaded = false;
		g_alReplay.Clear();
		g_bPlayReplay = false;
		SetDefaultBotName(client);
		MoveToSpec(client);
		
		return;
	}
	
	if (g_alReplay == null) // if no replay is loaded
	{
		return;
	}
	
	MoveToClimber(client);
	SetEntityMoveType(client, MOVETYPE_NOCLIP);
	SetEntProp(client, Prop_Data, "m_CollisionGroup", 2);
	
	float vecPosition[3];
	vecPosition[0] = g_alReplay.Get(g_iPlaybackTick, 0);
	vecPosition[1] = g_alReplay.Get(g_iPlaybackTick, 1);
	vecPosition[2] = g_alReplay.Get(g_iPlaybackTick, 2);
	
	float vecAngles[3];
	vecAngles[0] = g_alReplay.Get(g_iPlaybackTick, 3);
	vecAngles[1] = g_alReplay.Get(g_iPlaybackTick, 4);
	vecAngles[2] = g_alReplay.Get(g_iPlaybackTick, 5);
	
	int weapon = GetPlayerWeaponSlot(client, 1);
	if (weapon != -1)
	{
		SetEntPropEnt(client, Prop_Send, "m_hActiveWeapon", weapon);
	}
	
	if ((g_iPlaybackTick == 0 || g_iPlaybackTick == g_alReplay.Length - 1) && !g_bInBreather)
	{
		// Start the breather period
		g_bInBreather = true;
		g_fBreatherStartTime = GetEngineTime();
	}
	
	if (GetEngineTime() - g_fBreatherStartTime > BREATHER_TIME)
	{
		g_bInBreather = false;
		g_fBreatherStartTime = -1.0;
	}
	
	// set bot stuff even while in breather
	int replayButtons = g_alReplay.Get(g_iPlaybackTick, 6);
	int replayFlags = g_alReplay.Get(g_iPlaybackTick, 7);
	int envAttributes = PlayerEnvAttributes(client);
	
	if (FindConVar("kz_sv_movementmode_csgo").BoolValue)
	{
		envAttributes |= PLAYER_ENV_ATTRIBUTES_CSGOMOVEMENT;
	}
	
	buttons |= replayButtons & RPBUTTONS_BITFILTER;
	
	if (replayFlags & FL_DUCKING && ~buttons & IN_DUCK)
	{
		if (IsPlayerInCrouchTunnel(client, vecPosition))
		{
			if (~replayFlags & FL_ONGROUND && envAttributes & PLAYER_ENV_ATTRIBUTES_CSGOMOVEMENT)
			{
				vecPosition[2] -= 34.0;
			}
			buttons |= IN_DUCK;
		}
		else if (~replayFlags & FL_ONGROUND && envAttributes & PLAYER_ENV_ATTRIBUTES_CSGOMOVEMENT)
		{
			vecPosition[2] -= 18.0;
		}
	}
	
	if (replayFlags & FL_ONGROUND)
	{
		SetEntityMoveType(client, MOVETYPE_WALK);
		SetEntityFlags(client, GetEntityFlags(client) | FL_ONGROUND);
	}
	else
	{
		SetEntityMoveType(client, MOVETYPE_NOCLIP);
		SetEntityFlags(client, GetEntityFlags(client) & ~FL_ONGROUND);
	}
	
	TeleportEntity(client, NULL_VECTOR, vecAngles, NULL_VECTOR);
	SetEntPropVector(client, Prop_Data, "m_vecOrigin", vecPosition);
	SetEntPropVector(client, Prop_Data, "m_vecAbsOrigin", vecPosition);
	
	if (g_bInBreather)
	{
		return;
	}
	g_iPlaybackTick++;
}