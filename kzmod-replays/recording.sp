

void SaveRecording(int client, char[] szCourse, int courseLen, int timeInCentiSeconds, bool endurance)
{
	// without .kzreplay file extension
	char szReplayName[MAX_COURSE_SIZE + 64];
	Format(szReplayName, sizeof(szReplayName), "%s_%s_", szCourse, g_bTPRun[client] ? "TP" : "PRO");
	
	char path[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path, sizeof(path), "%s/%s", REPLAY_PATH_SM, g_szMap);
	if (!DirExists(path))
	{
		BuildPath(Path_SM, path, sizeof(path), "%s", REPLAY_PATH_SM);
		if (!DirExists(path))
		{
			CreateDirectory(path, 511);
		}
		
		BuildPath(Path_SM, path, sizeof(path), "%s/%s", REPLAY_PATH_SM, g_szMap);
		if (!DirExists(path))
		{
			CreateDirectory(path, 511);
		}
	}
	else
	{
		if (!endurance)
		{
			// check if this replay is faster than any other replay
			int lowestTime = FindLowestTime(path, szReplayName);
			
			if (lowestTime > 0 && lowestTime <= timeInCentiSeconds)
			{
				return;
			}
		}
		else
		{
			// check if this replay is bigger endurance than any other replay
			int highestTime = FindHighestTime(path, szReplayName);
			
			if (highestTime > 0 && highestTime >= timeInCentiSeconds)
			{
				return;
			}
		}
	}
	
	char szKeyName[MAX_COURSE_SIZE];
	FormatEx(szKeyName, sizeof(szKeyName), "%s%s", szCourse, g_bTPRun[client] ? "TP" : "PRO");
	g_smCourseTimes.SetValue(szKeyName, timeInCentiSeconds);
	
	Format(path, sizeof(path), "%s/%s%i.kzreplay", path, szReplayName, timeInCentiSeconds);
	
	if (FileExists(path))
	{
		DeleteFile(path);
	}
	
	File file = OpenFile(path, "wb");
	if (file == null)
	{
		LogError("Failed to create/open replay file to write to: \"%s\".", path);
		return;
	}
	
	// Prepare more data
	char steamID2[24];
	char alias[MAX_NAME_LENGTH];
	
	GetClientAuthId(client, AuthId_Steam2, steamID2, sizeof(steamID2));
	GetClientName(client, alias, sizeof(alias));
	int arrayLength = g_alRecording[client].Length;
	int teleportsUsed = view_as<int>(g_bTPRun[client]);
	
	// Write header
	file.WriteInt32(REPLAY_MAGIC_NUMBER);
	file.WriteInt8(REPLAY_FMT_VERSION);
	file.WriteInt8(strlen(g_szMap));
	file.WriteString(g_szMap, false);
	file.WriteInt8(courseLen);
	file.WriteString(szCourse, false);
	file.WriteInt32(teleportsUsed);
	file.WriteInt32(GetSteamAccountID(client));
	file.WriteInt8(strlen(steamID2));
	file.WriteString(steamID2, false);
	file.WriteInt8(strlen(alias));
	file.WriteString(alias, false);
	file.WriteInt32(arrayLength);
	file.WriteInt32(GetTime()); // unix 32 bit time
	file.WriteInt32(timeInCentiSeconds);
	
	// Write tick data
	any tickData[REPLAY_BLOCK_SIZE];
	for (int i = 0; i < arrayLength; i++)
	{
		for (int block; block < REPLAY_BLOCK_SIZE; block++)
		{
			tickData[block] = g_alRecording[client].Get(i, block);
		}
		file.Write(tickData, REPLAY_BLOCK_SIZE, 4);
	}
	delete file;
}

public void OnPlayerRunCmdPost_Recording(int client, int buttons, const float angles[3])
{
	g_iTimerRunning[client] = GetEntProp(client, Prop_Send, "Timer_Active");
	
	// reset timer to valid, because timer stopped
	if (g_iTimerRunning[client] == TIMER_STATE_INVISIBLE)
	{
		g_bInvalidTimer[client] = false;
	}
	
	if (!IsValidMoveType(client) || FindConVar("sv_cheats").IntValue != 0)
	{
		g_bInvalidTimer[client] = true;
	}
	
	if (g_iTimerRunning[client] == TIMER_STATE_ACTIVE && !g_bInvalidTimer[client])
	{
		g_bRecordRun[client] = true;
	}
	else
	{
		g_bRecordRun[client] = false;
	}
	
	if (g_bRecordRun[client])
	{
		float origin[3];
		GetClientAbsOrigin(client, origin);
		
		if (g_alRecording[client] == null)
		{
			g_alRecording[client] = new ArrayList(REPLAY_BLOCK_SIZE);
			LogError("[kzmod-replays] Recording should not be null. Probably happened because of loading the plugin late and as a consequence player replays may be incomplete.");
		}
		
		int tick = GetArraySize(g_alRecording[client]);
		
		g_alRecording[client].Resize(tick + 1);
		
		g_alRecording[client].Set(tick, origin[0], 0);
		g_alRecording[client].Set(tick, origin[1], 1);
		g_alRecording[client].Set(tick, origin[2], 2);
		g_alRecording[client].Set(tick, angles[0], 3);
		g_alRecording[client].Set(tick, angles[1], 4);
		g_alRecording[client].Set(tick, angles[2], 5);
		g_alRecording[client].Set(tick, buttons, 6);
		g_alRecording[client].Set(tick, GetEntityFlags(client), 7);
	}
}